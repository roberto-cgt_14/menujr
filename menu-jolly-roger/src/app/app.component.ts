import { Router } from '@angular/router';
import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Nx-DesaplicacionPagos';



  options = [
    { description: 'Favoritos', route: './demo' },
    { description: 'Reportes', route: './demo'},
    {
      description: 'Expediente',
      route: '/demo',
      items: [
        {description: 'Lorem', route: './demo'},
        {description: 'Ipsum', route: './demo'}
      ]
    },
    { description: 'Catalogo', route: '/demo'},
    {
      description: 'Cotizador',
      route: '/demo',
      items: [
        {description: 'Lorem', route: './logs'},
        {description: 'Ipsum', route: './demo'}
      ]
    },
    { description: 'Cotizador', route: '/demo'}
  ];


  progress = 70;

  constructor(public router: Router){

  }
  ngOnInit(){



  }
}
