import { Router } from '@angular/router';
import { UsuarioI } from './../../models/login.interface';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private api: LoginService,private router: Router) { }

  error: boolean = false;
  loginForm = new FormGroup({
    username: new FormControl(""),
    password: new FormControl(""),
  });

  ngOnInit() {
    this.api.getUsers().subscribe(data=>console.log(data));
  }

  loginPost(form: UsuarioI) {
    console.log(form);
    this.api.login(form).subscribe((data) => {
      console.log("data",data)
       localStorage.setItem("user",JSON.stringify(data));

      if (data) {
        this.router.navigate(["/home"]);
      } else {
        this.error = true;
      }
    });
  }
}
