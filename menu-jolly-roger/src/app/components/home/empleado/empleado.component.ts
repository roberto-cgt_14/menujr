import { EditComponent } from './edit/edit.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertService, ConfirmationService } from '@next/nx-controls-common';
import { EmpleadoService } from './../../../services/empleado.service';
import { EmpleadoI } from './../../../models/empleado.interface';
import { Component, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {



  empleados: EmpleadoI[];
  columns: String[] = ['brm', 'nombre', 'foto', 'puesto','acciones'];
  usuario = null;
  file: File | String;

  newForm= new FormGroup({
    nombre: new FormControl("", Validators.required),
    brm: new FormControl("", Validators.required),
    puesto: new FormControl("Selecciona un puesto..", Validators.required),
    foto: new FormControl("", Validators.required),
  });


  constructor(private api: EmpleadoService, private confirmationService: ConfirmationService,
              private alertService: AlertService,private dialogRef: MatDialog) { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem("user"));
    this.api.getAllEmpleados().subscribe(data=>this.empleados=data)
    console.log(this.empleados)
  }

  postForm(form: EmpleadoI){
    const formData = new FormData();
    formData.append("brm", this.newForm.get("brm").value);
    formData.append("nombre", this.newForm.get("nombre").value);
    formData.append("puesto", this.newForm.get("puesto").value);
    formData.append("foto", this.newForm.get("foto").value);
    this.api.postEmpleado(formData).subscribe((data) => {
      if (data) {
        this.api.getAllEmpleados().subscribe(data=>this.empleados=data)
        this.newForm.reset();
        this.alertService.success("Se ha guardado correctamente el empleado");
      }

      else{
          this.alertService.error("Ocurrio un error al momento de guardar");
      }
    },(err=>{
      this.alertService.error("Ocurrio un error al momento de guardar");
    }));
  }


  onChangeFoto(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.newForm.get("foto").setValue(file);
    }
  }

  dialogEdit(empleado){
    const formData = new FormData();
      const dialogRef = this.dialogRef.open(EditComponent,{width:'350px',data: empleado})
      dialogRef.afterClosed().subscribe(res=>{
        formData.append("brm", res.brm);
        formData.append("nombre", res.nombre);
        formData.append("puesto", res.puesto);
        formData.append("foto", res.foto);


        if (res) {
         if (res.foto = empleado.foto) {
           const url = `data:image/jpg;base64,${res.foto}`
           fetch(url).then(res=>res.blob()).then(blob => {
             this.file = new File([blob],"File name",{type:"image/jpg"})
             res.foto = this.file;
           })
         }
         this.api.putEmpleado(empleado.id,formData).subscribe(data=>{
         },(err)=>{
           console.log(err)
           if (err.statusText==="OK" && err.status === 200) {
            this.alertService.success("Se ha actualizado el empleado correctamente");
            this.api.getAllEmpleados().subscribe(data=>this.empleados=data)
           }
           else{
            this.alertService.error("Ocurrió un error, por favor cambie de imagen")
           }
         });

        }
        else{
          console.log("Cerrar")
        }
      })
  }









  dialogDelete(event){
    console.log(event)
    this.confirmationService.confirm({
      message: '¿Esta seguro de querer realizar esta acción?',
      lblOkBtn: 'Si',
      lblCancelBtn: 'Cancelar',
      accept: () => {
        this.api.deleteEmpleado(event).subscribe(
          (data) => console.log(data),
          (err) => {
            if (err.statusText === "OK") {
              this.alertService.success("Se ha eliminado correctamente el empleado");
              this.api.getAllEmpleados().subscribe(data=>this.empleados=data)
            }
            else{
              this.alertService.error("Occurió un error al momento de eliminar el empleado");
            }
          }
        );
      },
      reject: () => {
          console.log('Canceled!');
      }
    });

  }

}
