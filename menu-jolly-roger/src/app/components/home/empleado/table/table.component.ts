import { EmpleadoI } from './../../../../models/empleado.interface';
import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() empleados: EmpleadoI[];
  @Input() columns: String[];
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();
  constructor() { }

  ngOnInit() {

  }
  editar(empleado){

    this.edit.emit(empleado)
  }
  eliminar(id){
    this.delete.emit(id)
  }

}
