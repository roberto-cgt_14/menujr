import { EmpleadoService } from './../../../../services/empleado.service';
import { EmpleadoI } from './../../../../models/empleado.interface';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],

})
export class EditComponent implements OnInit {




  constructor(public dialogRef: MatDialogRef<EditComponent>,@Inject(MAT_DIALOG_DATA) public message,
  private api: EmpleadoService) { }


  editForm = new FormGroup({
    nombre: new FormControl("", Validators.required),
    brm: new FormControl("", Validators.required),
    puesto: new FormControl("Selecciona un puesto..", Validators.required),
    foto: new FormControl("", Validators.required),
  })


  ngOnInit() {
    this.editForm.setValue({

      nombre: this.message.nombre,
      brm: this.message.brm,
      puesto: this.message.puesto,
      foto: this.message.foto,
    })
  }

  onChangeFoto(event){
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.editForm.get("foto").setValue(file);
    }
  }


  onClickNo():void{
    console.log("lci")
    this.dialogRef.close();
  }

}
