import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from '@next/nx-controls-common';
import { Router } from '@angular/router';
import { LoadingService } from '@next/nx-core/services/loading.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: []
})
export class HomeComponent implements OnInit {
  leftContent: any = [
    { description: 'Menú', isTitle: true },

  ];
  usuario = null;
  constructor(public router: Router,) {}

ngOnInit() {
  this.usuario = JSON.parse(localStorage.getItem("user"));
  if (this.usuario.rol == "jugador") {
    this.leftContent.push({ description: 'Juego', isTitle: false, route:'home/jugador'});

  } else {
    this.leftContent.push({ description: 'Empleados', isTitle: false, route:'home/empleado'});

  }

}
}

