import { ResponseI } from './../models/response.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmpleadoI } from '../models/empleado.interface';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(private http:HttpClient) { }

  getAllEmpleados(): Observable<EmpleadoI[]> {
    return this.http.get<EmpleadoI[]>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados"
    );
  }

  deleteEmpleado(id): Observable<ResponseI> {
    return this.http.delete<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id
    );
  }

  postEmpleado(form) {
    return this.http.post<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados",
      form
    );
  }
  putEmpleado(id, form) {
    return this.http.put<ResponseI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id,
      form
    );
  }

  getEmpleado(id) {
    return this.http.get<EmpleadoI>(
      "https://app-aplicacion-empresarial.herokuapp.com/api/empleados/" + id
    );
  }
}
