import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(form) {
    return this.http.post(
      "https://app-login-back.herokuapp.com/api/login",
      form
    );
  }

  getUsers(){
    return this.http.get("https://app-login-back.herokuapp.com/api/users")
  }
}
