import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { FrameworkModule} from '@next/nx-core';
import { CommonsModule} from '@next/nx-controls-common';
import { CommonModule } from '@angular/common';
import { HomeComponent } from 'src/app/components/home/home.component';

import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatInputModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule, MatRadioModule,
  MatDatepickerModule, MatNativeDateModule, MatTableModule, MatIconModule, MatButtonToggleModule, MatSlideToggleModule,
  MatTabsModule, MatCheckboxModule, MatMenuModule, MatStepperModule, MatPaginatorModule,MatCardModule,MatDialogModule} from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';


import { MatFormFieldModule} from '@angular/material';
import { MatTreeModule } from '@angular/material/tree';

import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { LoginComponent } from './components/login/login.component';
import { JugadorComponent } from './components/home/jugador/jugador.component';
import { EmpleadoComponent } from './components/home/empleado/empleado.component';
import { TableComponent } from './components/home/empleado/table/table.component';
import { EditComponent } from './components/home/empleado/edit/edit.component';






const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '',
  application: 'DESAPLICACIONREVERSA',
  applicationTitle: 'Desaplicacion de Pagos'
};


@NgModule({
  declarations: [
    AppComponent,
    RootComponent,

    HomeComponent,

    LoginComponent,

    JugadorComponent,

    EmpleadoComponent,

    TableComponent,

    EditComponent,



  ],
  entryComponents: [EditComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot(),
    MatTabsModule,
    FormsModule,
    CdkTableModule,
    CdkTreeModule,
    MatInputModule, MatSelectModule, MatTooltipModule, MatProgressSpinnerModule, MatRadioModule,
    MatDatepickerModule, MatNativeDateModule, MatTableModule, MatIconModule, MatButtonToggleModule, MatSlideToggleModule,
  MatTabsModule, MatCheckboxModule, MatMenuModule, MatStepperModule, MatPaginatorModule,
  MatExpansionModule,
  MatFormFieldModule,
  ReactiveFormsModule,
  MatListModule,
  MatTreeModule,
  MatCardModule,
  MatDialogModule

  ],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
