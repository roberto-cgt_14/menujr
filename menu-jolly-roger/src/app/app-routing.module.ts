import { JugadorComponent } from './components/home/jugador/jugador.component';
import { EmpleadoComponent } from './components/home/empleado/empleado.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';



const routes: Routes = [
{path: '', redirectTo:'/home', pathMatch:'full'},
 {path: 'login', component: LoginComponent, },
  {
   path: 'home', component: HomeComponent,
   children: [
    {
      path: 'jugador', component:JugadorComponent,
     },

     {
      path: 'empleado', component: EmpleadoComponent,
     }
   ]
  },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
